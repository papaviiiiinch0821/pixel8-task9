<?php

namespace Tests\Api;

use Tests\Support\ApiTester;

class TodoListFailCest
{
    // Test API to not Insert Data
    public function iShouldNotInsertData(ApiTester $I)
    {
         // Set the Content-Type header to application/x-www-form-urlencoded
         $I->haveHttpHeader('Content-Type', 'application/x-www-form-urlencoded');

        // Test scenario where the payload for insertion is empty
        $I->sendPOST('/API.php', []);
        $I->seeResponseCodeIs(200);
        $I->seeResponseContainsJson(['status' => 'failed']);
    }

    // Test API to not Get Data
    public function iShouldNotGetData(ApiTester $I)
    {
        // Set the Content-Type header to application/json
        $I->haveHttpHeader('Content-Type', 'application/json');

        // Test scenario where the GET request for a single item fails due to invalid ID
        $I->sendGET('/API.php?id=invalid_id');
        $I->seeResponseCodeIs(200);
        $I->seeResponseContainsJson(['status' => 'failed']);
    }

    // Test API to not Update Data
    public function iShouldNotUpdateData(ApiTester $I)
    {
        // Set the Content-Type header to application/json
        $I->haveHttpHeader('Content-Type', 'application/json');

        // Test scenario where the PUT request fails due to missing or invalid payload
        $I->sendPUT('/API.php/123', []);
        $I->seeResponseCodeIs(200);
        $I->seeResponseContainsJson(['status' => 'failed']);
    }

    // Test API to not Delete Data
    public function iShouldNotDeleteData(ApiTester $I)
    {
        // Set the Content-Type header to application/json
        $I->haveHttpHeader('Content-Type', 'application/json');

        // Test scenario where the DELETE request fails due to missing or invalid ID
        $I->sendDELETE('/API.php/invalid_id');
        $I->seeResponseCodeIs(200);
        $I->seeResponseContainsJson(['status' => 'failed']);
    }
}

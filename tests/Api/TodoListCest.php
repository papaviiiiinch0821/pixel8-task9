<?php


namespace Tests\Api;

use Tests\Support\ApiTester;

class TodoListCest
{

    private $id;

    // // Test API to Insert One Data
    public function iShouldInsertData(ApiTester $I)
    {
        // Set the Content-Type header to application/x-www-form-urlencoded
        $I->haveHttpHeader('Content-Type', 'application/x-www-form-urlencoded');

        $I->sendPost('/API.php', [
            'createTaskTitle' => ['Basketball'],
            'createTaskName' => ['Dribble'],
            'createTaskTime' => ['2024-04-04'],
        ]);

        // Check the response
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['status' => 'success']);

        // Extract the ID from the response
        $response = json_decode($I->grabResponse(), true);
        $this->id = $response['id'];
    }

    // // // Test API to Insert Multiple Data
    // public function iShouldInsertMultipleData(ApiTester $I)
    // {
    //     // Set the Content-Type header to application/x-www-form-urlencoded
    //     $I->haveHttpHeader('Content-Type', 'application/x-www-form-urlencoded');

    //     // Send a POST request 
    //     $I->sendPost('/API.php', [
    //         'createTaskTitle' => ['Basketball', 'Football', 'Swimming', 'Running', 'Cycling'],
    //         'createTaskName' => ['Dribble', 'Passing', 'Breaststroke', 'Sprinting', 'Mountain Biking'],
    //         'createTaskTime' => ['2024-04-04', '2024-04-05', '2024-04-06', '2024-04-07', '2024-04-08'],
    //     ]);

    //     // Check the response
    //     $I->seeResponseCodeIs(200);
    //     $I->seeResponseIsJson();
    //     $I->seeResponseContainsJson(['status' => 'success']);
    // }

    // Test API to Get All Data
    public function iShouldGetAllData(ApiTester $I)
    {
        // Set the Content-Type header to application/json
        $I->haveHttpHeader('Content-Type', 'application/json');

        // Send a GET request to retrieve all data
        $I->sendGet('/API.php');

        // Check the response
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
    }

    // Test API to Get One Data
    public function iShouldGetOneData(ApiTester $I)
    {
        // Set the Content-Type header to application/json
        $I->haveHttpHeader('Content-Type', 'application/json');


        // Send a GET request to retrieve data with the specified ID
        $I->sendGet('/API.php?id=' . $this->id);

        // Check the response
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
    }

    // Test API to Update Data
    public function iShouldUpdateData(ApiTester $I)
    {
        // Set the Content-Type header to application/json
        $I->haveHttpHeader('Content-Type', 'application/json');

        // Send a PUT request to update data with the specified ID
        $I->sendPut('/API.php/' . $this->id, [
            'id' => $this->id,
            'task_title' => 'Volleyball',
            'task_name' => 'Spike',
            'time' => '2024-04-21',
        ]);

        // Check the response
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
    }

    // Test API to Delete Data
    public function iShouldDeleteData(ApiTester $I)
    {
        // Set the Content-Type header to application/json
        $I->haveHttpHeader('Content-Type', 'application/json');

        // Send a DELETE request to delete data with the specified ID
        $I->sendDelete('/API.php/' . $this->id, [
            'id' => $this->id
        ]);

        // Check the response
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
    }
}

<?php
include 'connection.php';
?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Pixel8-Task9</title>
    <!-- Bootstrap -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap Font Icon -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
    <!-- Alertify -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/alertify.min.css" />
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/bootstrap.min.css" />

</head>

<body>
    <h3 class="text-center pt-3">ToDo Application | Requst & Respond | REST API </h3>

    <!-- Create Task Modal -->
    <div class="modal fade" id="createTaskModal" aria-hidden="true" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title w-100 text-center fs-5">Create Task</h1>
                </div>
                <div class="modal-body">
                    <form id="createTaskForm">
                        <!-- Duplicate this form onclick of Add Another Task -->
                        <div id="show-item">
                            <div class="row g-2 py-2">
                                <div class="col-sm-3">
                                    <input type="text" class="form-control bg-light" name="createTaskTitle[]" placeholder="Enter Task Title.">
                                </div>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control bg-light" name="createTaskName[]" placeholder="Enter Task Name.">
                                </div>
                                <div class="col-sm-3">
                                    <input type="datetime-local" class="form-control bg-light" name="createTaskTime[]">
                                </div>
                                <div class="col-sm-3">
                                    <button class="btn btn-outline-success w-100 addTask">Add More</button>
                                </div>
                            </div>
                        </div>
                        <!-- Duplicate this form onclick of Add Another Task -->

                        <div class="modal-footer mt-2 pb-0 px-0">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-primary">Create</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Edit Task Modal -->
    <div class="modal fade" id="editTaskModal" aria-hidden="true" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title w-100 text-center fs-5">Edit Task</h1>
                </div>
                <div class="modal-body">
                    <form id="editTaskForm">
                        <div class="row g-2 py-2">
                            <input type="hidden" id="editTaskID" name="editTaskID">
                            <div class="col-sm-4">
                                <input type="text" class="form-control bg-light" id="editTaskTitle" name="editTaskTitle" placeholder="Enter Task Title.">
                            </div>
                            <div class="col-sm-4">
                                <input type="text" class="form-control bg-light" id="editTaskName" name="editTaskName" placeholder="Enter Task Name.">
                            </div>
                            <div class="col-sm-4">
                                <input type="datetime-local" class="form-control bg-light" id="editTaskTime" name="editTaskTime">
                            </div>
                        </div>
                        <div class="modal-footer mt-2 pb-0 px-0">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-primary">Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Delete Task Modal -->
    <div class="modal fade" id="deleteTaskModal" aria-hidden="true" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title w-100 text-center fs-5">Delete Task?</h1>
                </div>
                <div class="modal-body">
                    <form id="deleteTaskForm">
                        <div class="row">
                            <input type="hidden" id="deleteTaskID" name="deleteTaskID">
                            <div class="col">
                                <p>Delete task "<strong id="deleteTaskTitle"></strong>"?</p>
                            </div>
                        </div>
                        <div class="modal-footer mt-2 pb-0 px-0">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-primary">Delete</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <section>
        <div class="row">
            <!-- Ongoing Tasks -->
            <div class="col-lg-6 col-sm-12">
                <div id="ongoingTaks" class="card m-2">
                    <div class="card-header d-flex align-items-center fw-bold fs-3">
                        <h3 class="fw-bold w-100 m-0">
                            Ongoing Tasks
                        </h3>
                        <button type="button" class="btn btn-dark rounded-cirlce flex-shrink-1" data-bs-toggle="modal" data-bs-target="#createTaskModal">
                            <span class="bi bi-plus-circle-fill" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Create Task"></span>
                        </button>
                    </div>
                    <div class="card-body">
                        <div class="accordion" id="accordionInprogress">
                            <?php
                            $db->where('status', 'Inprogress');
                            $inprogress = $db->get('tbl_to_do_list');

                            if ($db->count > 0) {
                                foreach ($inprogress as $todo) {
                            ?>
                                    <div class="accordion-item">
                                        <h2 class="accordion-header" id="heading<?= $todo['id'] ?>">
                                            <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapse<?= $todo['id'] ?>" aria-expanded="false" aria-controls="collapse<?= $todo['id'] ?>">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" value="<?= $todo['id'] ?>" id="flexCheckDefault<?= $todo['id'] ?>" style="cursor: pointer;">
                                                    <label class="form-check-label" for="flexCheckDefault<?= $todo['id'] ?>">
                                                        <p class="p-0 m-0"><?= $todo['task_title'] ?></p>
                                                    </label>
                                                </div>
                                            </button>
                                        </h2>
                                        <div id="collapse<?= $todo['id'] ?>" class="accordion-collapse collapse" aria-labelledby="heading<?= $todo['id'] ?>" data-bs-parent="#accordionInprogress">
                                            <div class="accordion-body d-flex">
                                                <div class="w-100">
                                                    <p class="p-0 m-0"><?= $todo['task_name'] ?></p>
                                                    <strong class="p-0 m-0">Due: <?= $todo['time'] ?></strong>
                                                </div>
                                                <div class="dropdown">
                                                    <span class="bi bi-three-dots fs-5" type="button" data-bs-toggle="dropdown" aria-expanded="false"></span>
                                                    <ul class="dropdown-menu">
                                                        <li><button value="<?= $todo['id'] ?>" class="editTaskButton dropdown-item">Edit</button></li>
                                                        <li><button value="<?= $todo['id'] ?>" class="deleteTaskButton dropdown-item">Delete</button></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php
                                }
                            } else {
                                ?>
                                <h5 class="text-center text-muted">No ongoing tasks yet.</h5>
                            <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Ongoing Tasks -->

            <!-- Done Tasks -->
            <div class="col-lg-6 col-sm-12">
                <div class="card m-2">
                    <div class="card-header fw-bold fs-3">
                        Done Tasks
                    </div>
                    <div class="card-body">
                        <div class="accordion" id="accordionDone">
                            <?php
                            $db->where('status', 'Done');
                            $done = $db->get('tbl_to_do_list');

                            if ($db->count > 0) {
                                foreach ($done as $todo) {
                            ?>
                                    <div class="accordion-item">
                                        <h2 class="accordion-header" id="heading<?= $todo['id'] ?>">
                                            <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapse<?= $todo['id'] ?>" aria-expanded="false" aria-controls="collapse<?= $todo['id'] ?>">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" value="<?= $todo['id'] ?>" id="flexCheckDefault<?= $todo['id'] ?>" style="cursor: pointer;" checked>
                                                    <label class="form-check-label" for="flexCheckDefault<?= $todo['id'] ?>">
                                                        <s class="p-0 m-0"><?= $todo['task_title'] ?></s>
                                                    </label>
                                                </div>
                                            </button>
                                        </h2>
                                        <div id="collapse<?= $todo['id'] ?>" class="accordion-collapse collapse" aria-labelledby="heading<?= $todo['id'] ?>" data-bs-parent="#accordionInprogress">
                                            <div class="accordion-body d-flex">
                                                <div class="w-100">
                                                    <p class="p-0 m-0"><?= $todo['task_name'] ?></p>
                                                    <strong class="p-0 m-0">Due: <?= $todo['time'] ?></strong>
                                                </div>
                                                <div class="dropdown">
                                                    <span class="bi bi-three-dots fs-5" type="button" data-bs-toggle="dropdown" aria-expanded="false"></span>
                                                    <ul class="dropdown-menu">
                                                        <li><button value="<?= $todo['id'] ?>" class="editTaskButton dropdown-item">Edit</button></li>
                                                        <li><button value="<?= $todo['id'] ?>" class="deleteTaskButton dropdown-item">Delete</button></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php
                                }
                            } else {
                                ?>
                                <h5 class="text-center text-muted">No done tasks yet.</h5>
                            <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Done Tasks -->
        </div>
    </section>

    <!-- JQuery -->
    <script src="https://code.jquery.com/jquery-3.7.1.min.js"></script>
    <!-- Bootstrap -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
    <!-- Alertify -->
    <script src="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/alertify.min.js"></script>
    <!-- Functinos -->
    <script src="functions.js"></script>
</body>

</html>
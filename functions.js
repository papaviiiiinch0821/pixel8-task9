// Enable Tooltips
const tooltipTriggerList = document.querySelectorAll(
  '[data-bs-toggle="tooltip"]'
);
const tooltipList = [...tooltipTriggerList].map(
  (tooltipTriggerEl) => new bootstrap.Tooltip(tooltipTriggerEl)
);
// Enable Tooltips

// Create Task - Add Another Task
$(document).on("click", ".addTask", function (e) {
  e.preventDefault();
  $("#show-item").prepend(`
        <div class="row g-2 py-2">
            <div class="col-sm-3">
                <input type="text" class="form-control bg-light" name="createTaskTitle[]" placeholder="Enter Task Title.">
            </div>
            <div class="col-sm-3">
                <input type="text" class="form-control bg-light" name="createTaskName[]" placeholder="Enter Task Name.">
            </div>
            <div class="col-sm-3">
                <input type="datetime-local" class="form-control bg-light" name="createTaskTime[]">
            </div>
            <div class="col-sm-3">
                <button class="btn btn-outline-danger w-100 removeTask">Remove</button>
            </div>
        </div>
    `);
});
// Create Task - Add Another Task

// Create Task - Remove Added Task
$(document).on("click", ".removeTask", function (e) {
  e.preventDefault();
  let row_item = $(this).parent().parent();
  $(row_item).remove();
});
// Create Task - Remove Added Task

// Create Task
$("#createTaskForm").on("submit", function (e) {
  e.preventDefault();

  var $form = $(this); // Capture the reference to the form

  // Check if all input fields are filled and complete
  var isValid = true;
  $form
    .find("input[type='text'], input[type='datetime-local']")
    .each(function () {
      if ($(this).val() === "") {
        isValid = false;
        return false; // exit the loop if any field is empty
      }
    });

  if (!isValid) {
    // If any field is empty, show an error using alertify
    alertify.set("notifier", "position", "top-right");
    alertify.warning("Please complete all fields.");
    return;
  }

  // Serialize form data
  var formData = $form.serialize();

  $.ajax({
    type: "POST",
    url: "./API.php",
    data: formData,
    success: function (response) {
      console.log(response);
      // Parse JSON response
      var res = jQuery.parseJSON(response);

      if (res.status == "success") {
        // Notify User
        alertify.set("notifier", "position", "top-right");
        alertify.success(res.message);
        setTimeout(() => {
          // Hide Modal
          $("#createTaskModal").modal("hide");
          // Reload List
          $("#accordionInprogress").load(" #accordionInprogress > *");
          $("#accordionDone").load(" #accordionDone > *");
          // Reset Form
          $("#createTaskForm")[0].reset();
        }, 2000);
      } else {
        // Notify User
        alertify.set("notifier", "position", "top-right");
        alertify.error(res.message);
      }
    },
  });
});
// Create Task

// Edit Task - Retrive One
$(document).on("click", ".editTaskButton", function (e) {
  e.preventDefault();
  var id = $(this).val();
  $.ajax({
    type: "GET",
    url: "./API.php?id=" + id,
    success: function (response) {
      var res = jQuery.parseJSON(response);

      if (res.status == "success") {
        $("#editTaskID").val(res.data.id);
        $("#editTaskTitle").val(res.data.task_title);
        $("#editTaskName").val(res.data.task_name);
        $("#editTaskTime").val(res.data.time);
        $("#editTaskModal").modal("show");
      } else {
        alertify.set("notifier", "position", "top-right");
        alertify.error(res.message);
      }
    },
  });
});
// Edit Task - Retrive One

// Update Task
$("#editTaskForm").on("submit", function (e) {
  e.preventDefault();

  var $form = $(this); // Capture the reference to the form

  // Check if all input fields are filled and complete
  var isValid = true;
  $form
    .find("input[type='text'], input[type='datetime-local']")
    .each(function () {
      if ($(this).val() === "") {
        isValid = false;
        return false; // exit the loop if any field is empty
      }
    });

  if (!isValid) {
    // If any field is empty, show an error using alertify
    alertify.set("notifier", "position", "top-right");
    alertify.warning("Please complete all fields.");
    return;
  }

  // Construct the data object to be sent in the PUT request
  var formData = {
    id: $("#editTaskID").val(),
    task_title: $("#editTaskTitle").val(),
    task_name: $("#editTaskName").val(),
    time: $("#editTaskTime").val(),
  };

  // Send the PUT request
  $.ajax({
    type: "PUT",
    url: "./API.php/" + formData.id,
    data: JSON.stringify(formData),
    success: function (response) {
      var res = jQuery.parseJSON(response);

      if (res.status == "success") {
        // Notify User
        alertify.set("notifier", "position", "top-right");
        alertify.success(res.message);
        setTimeout(() => {
          // Reload List
          $("#accordionInprogress").load(" #accordionInprogress > *");
          $("#accordionDone").load(" #accordionDone > *");
          // Hide Modal
          $("#editTaskModal").modal("hide");
        }, 2000);
      } else {
        // Handle failure response
        alertify.set("notifier", "position", "top-right");
        alertify.error(res.message);
      }
    },
    error: function (xhr, status, error) {
      // Handle error response
      alertify.set("notifier", "position", "top-right");
      alertify.error("Error: " + error);
    },
  });
});
// Update Task

// Update Task ("Inprogress" | "Done")
$(document).on("click", ".form-check-input  ", function () {
  var label = $(this).parent().find("label");
  var id = $(this).val();

  if ($(this).is(":checked")) {
    $.ajax({
      type: "PUT",
      url: "./API.php/" + id,
      data: JSON.stringify({
        // Convert data to JSON string
        id: id,
        status: "Done", // Update the status to 'Done'
      }),
      processData: false,
      contentType: "application/json", // Set content type to JSON
      success: function (response) {
        // Parse JSON response
        var res = jQuery.parseJSON(response);

        if (res.status == "success") {
          // Notify User
          alertify.set("notifier", "position", "top-right");
          alertify.success(res.message);
          // Strike Text
          label.find("p").replaceWith(function () {
            return $("<s>").html($(this).html());
          });
          setTimeout(() => {
            // Reload List
            $("#accordionInprogress").load(" #accordionInprogress > *");
            $("#accordionDone").load(" #accordionDone > *");
          }, 2000);
        } else {
          // Notify User
          alertify.set("notifier", "position", "top-right");
          alertify.error(res.message);
        }
      },
      error: function (xhr, status, error) {
        // Handle error response
        alertify.set("notifier", "position", "top-right");
        alertify.error("Error: " + error);
      },
    });
  } else {
    $.ajax({
      type: "PUT",
      url: "./API.php/" + id,
      data: JSON.stringify({
        // Convert data to JSON string
        id: id,
        status: "Inprogress", // Update the status back to 'Inprogress'
      }),
      processData: false,
      contentType: "application/json", // Set content type to JSON
      success: function (response) {
        // Parse JSON response
        var res = jQuery.parseJSON(response);

        if (res.status == "success") {
          // Notify User
          alertify.set("notifier", "position", "top-right");
          alertify.success(res.message);
          // Unstrike Text
          label.find("s").replaceWith(function () {
            return $("<p>").html($(this).html());
          });
          setTimeout(() => {
            // Reload List
            $("#accordionInprogress").load(" #accordionInprogress > *");
            $("#accordionDone").load(" #accordionDone > *");
          }, 2000);
        } else {
          // Notify User
          alertify.set("notifier", "position", "top-right");
          alertify.error(res.message);
        }
      },
      error: function (xhr, status, error) {
        // Handle error response
        alertify.set("notifier", "position", "top-right");
        alertify.error("Error: " + error);
      },
    });
  }
});
// Update Task ("Inprogress" | "Done")

// Confirm Delete Task
$(document).on("click", ".deleteTaskButton", function (e) {
  e.preventDefault();
  var id = $(this).val();
  $.ajax({
    type: "GET",
    url: "./API.php?id=" + id,
    success: function (response) {
      var res = jQuery.parseJSON(response);

      if (res.status == "success") {
        $("#deleteTaskID").val(res.data.id);
        $("#deleteTaskTitle").text(res.data.task_title);
        $("#deleteTaskModal").modal("show");
      } else {
        alertify.set("notifier", "position", "top-right");
        alertify.error(res.message);
      }
    },
  });
});
// Confirm Delete Task

// Delete Task
$("#deleteTaskForm").on("submit", function (e) {
  e.preventDefault();
  var id = $("#deleteTaskID").val();

  // Send the DELETE request
  $.ajax({
    type: "DELETE",
    url: "./API.php/" + id,
    data: JSON.stringify({ id: id }),
    success: function (response) {
      var res = jQuery.parseJSON(response);

      if (res.status == "success") {
        // Notify User
        alertify.set("notifier", "position", "top-right");
        alertify.success(res.message);
        setTimeout(() => {
          // Reload List
          $("#accordionInprogress").load(" #accordionInprogress > *");
          $("#accordionDone").load(" #accordionDone > *");
          // Hide Modal
          $("#deleteTaskModal").modal("hide");
        }, 2000);
      } else {
        // Handle failure response
        alertify.set("notifier", "position", "top-right");
        alertify.error(res.message);
      }
    },
    error: function (xhr, status, error) {
      // Handle error response
      alertify.set("notifier", "position", "top-right");
      alertify.error("Error: " + error);
    },
  });
});
// Delete Task

<?php
$servername = 'localhost';
$username = 'root';
$password = '';
$dbname = "rest_api_todolist";

require_once 'MysqliDb.php';

// Initialize MysqliDb object with your database connection details
$db = new MysqliDb($servername, $username, $password, $dbname);

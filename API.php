<?php

/**
 * Tells the browser to allow code from any origin to access
 */

use PhpParser\Node\Expr\Cast\Array_;

header("Access-Control-Allow-Origin: *");

/**
 * Tells browsers whether to expose the response to the frontend JavaScript code
 * when the request's credentials mode (Request.credentials) is include
 */
header("Access-Control-Allow-Credentials: true");

/**
 * Specifies one or more methods allowed when accessing a resource in response to a preflight request
 */
header("Access-Control-Allow-Methods: POST, GET, PUT, DELETE");

/**
 * Used in response to a preflight request which includes the Access-Control-Request-Headers to
 * indicate which HTTP headers can be used during the actual request
 */
header("Access-Control-Allow-Headers: Content-Type");

// Require MysliDB
require_once('MysqliDb.php');

class API
{
    private $db;

    // Connect to Database
    public function __construct()
    {
        $this->db = new MysqliDb('localhost', 'root', '', 'rest_api_todolist');
    }

    /**
     * HTTP GET Request
     *
     * @param $payload
     */
    public function httpGet($payload)
    {
        // Check payload if it's an array and not empty
        if (!is_array($payload) && !empty($payload)) {
            echo json_encode(array(
                'method' => 'GET',
                'status' => 'failed',
                'message' => 'Payload must be an array and not empty'
            ));
            return;
        }

        // Check if there's a specific condition to retrieve a single row
        if (isset($payload['id'])) {
            // Execute get query with specific condition
            $this->db->where('id', $payload['id']);
            $data = $this->db->getOne('tbl_to_do_List');

            // Check if query was successful
            if ($data) {
                echo json_encode(array(
                    'method' => 'GET',
                    'status' => 'success',
                    'data' => $data
                ));
            } else {
                echo json_encode(array(
                    'method' => 'GET',
                    'status' => 'failed',
                    'message' => 'Failed Fetch Request'
                ));
            }
        } else {
            // If no specific condition is provided, retrieve all data
            $data = $this->db->get('tbl_to_do_List');

            // Check if query was successful
            if ($this->db->count > 0) {
                echo json_encode(array(
                    'method' => 'GET',
                    'status' => 'success',
                    'data' => $data
                ));
            } else {
                echo json_encode(array(
                    'method' => 'GET',
                    'status' => 'failed',
                    'message' => 'Failed Fetch Request'
                ));
            }
        }
    }

    /**
     * HTTP POST Request
     *
     * @param $payload
     */

    // public function httpPost($payload)
    // {
    //     // Check if payload is not empty
    //     if (empty($payload)) {
    //         echo json_encode(array(
    //             'method' => 'POST',
    //             'status' => 'failed',
    //             'message' => 'Payload must not be empty',
    //             'data' => $payload
    //         ));
    //         return;
    //     }

    //     $data = []; // Initialize an empty array to hold the data

    //     // Loop through each set of data in the payload
    //     foreach ($payload['createTaskTitle'] as $index => $task_title) {
    //         // Create an associative array with the required keys and values
    //         $task_data = array(
    //             'task_title' => $task_title,
    //             'task_name' => $payload['createTaskName'][$index],
    //             'time' => $payload['createTaskTime'][$index],
    //             'status' => 'Inprogress'
    //         );

    //         // Append this associative array to the $data array
    //         $data[] = $task_data;
    //     }

    //     $ids = $this->db->insertMulti('tbl_to_do_list', $data);
    //     if ($ids) {
    //         echo json_encode(array(
    //             'method' => 'POST',
    //             'status' => 'success',
    //             'message' => 'Data inserted successfully',
    //             'data' => $data
    //         ));
    //     } else {
    //         echo json_encode(array(
    //             'method' => 'POST',
    //             'status' => 'failed',
    //             'message' => 'Data insert failed.',
    //         ));
    //     }
    // }
    public function httpPost($payload)
    {
        // Check if payload is not empty
        if (empty($payload)) {
            echo json_encode(array(
                'method' => 'POST',
                'status' => 'failed',
                'message' => 'Payload must not be empty',
                'data' => $payload
            ));
            return;
        }

        // Check if payload is an array and has more than one data
        if (is_array($payload) && count($payload['createTaskTitle']) > 1) {
            $data = []; // Initialize an empty array to hold the data

            // Loop through each set of data in the payload
            foreach ($payload['createTaskTitle'] as $index => $task_title) {
                // Create an associative array with the required keys and values
                $task_data = array(
                    'task_title' => $task_title,
                    'task_name' => $payload['createTaskName'][$index],
                    'time' => $payload['createTaskTime'][$index],
                    'status' => 'Inprogress'
                );

                // Append this associative array to the $data array
                $data[] = $task_data;
            }

            // Perform multiple insert of data
            $ids = $this->db->insertMulti('tbl_to_do_list', $data);
            if ($ids) {
                echo json_encode(array(
                    'method' => 'POST',
                    'status' => 'success',
                    'message' => 'Multiple Data created successfully',
                    'data' => $data
                ));
            } else {
                echo json_encode(array(
                    'method' => 'POST',
                    'status' => 'failed',
                    'message' => 'Data insert failed.',
                ));
            }
        } else {
            // Loop through each index of data in the payload to remove the [] brackets
            foreach ($payload['createTaskTitle'] as $index => $task_title) {
                // Create an associative array with the required keys and values
                $data = array(
                    'task_title' => $task_title,
                    'task_name' => $payload['createTaskName'][$index],
                    'time' => $payload['createTaskTime'][$index],
                    'status' => 'Inprogress'
                );
            }

            // Perform single insert of data
            $id = $this->db->insert('tbl_to_do_list', $data);
            if ($id) {
                echo json_encode(array(
                    'method' => 'POST',
                    'status' => 'success',
                    'message' => 'Single Data created successfully',
                    'data' => $data,
                    'id' => $id
                ));
            } else {
                echo json_encode(array(
                    'method' => 'POST',
                    'status' => 'failed',
                    'message' => 'Data insert failed.',
                ));
            }
        }
    }


    /**
     * HTTP PUT Request
     *
     * @param $id
     * @param $payload
     */
    public function httpPut($id, $payload)
    {
        // Check if id is not null or empty
        if (empty($id)) {
            echo json_encode(array(
                'method' => 'PUT',
                'status' => 'failed',
                'message' => 'ID cannot be empty'
            ));
            return;
        }

        // Check if payload is not empty
        if (empty($payload)) {
            echo json_encode(array(
                'method' => 'PUT',
                'status' => 'failed',
                'message' => 'Payload must not be empty'
            ));
            return;
        }

        // Check if the id passed in matches the id in the payload
        if ($id != $payload['id']) {
            echo json_encode(array(
                'method' => 'PUT',
                'status' => 'failed',
                'message' => 'ID in payload does not match provided ID'
            ));
            return;
        }

        // Execute update query
        $this->db->where('id', $id);
        $result = $this->db->update('tbl_to_do_List', $payload);

        // Check if query was successful
        if ($result) {
            echo json_encode(array(
                'method' => 'PUT',
                'status' => 'success',
                'message' => 'Task updated.',
                'data' => $id
            ));
        } else {
            echo json_encode(array(
                'method' => 'PUT',
                'status' => 'failed',
                'message' => 'Failed to Update Data'
            ));
        }
    }

    /**
     * HTTP DELETE Request
     *
     * @param $id
     * @param $payload
     */
    public function httpDelete($id, $payload)
    {
        // Check if id is not null or empty
        if (empty($id)) {
            echo json_encode(array(
                'method' => 'DELETE',
                'status' => 'failed',
                'message' => 'ID cannot be empty'
            ));
            return;
        }

        // Check if payload is not empty
        if (empty($payload)) {
            echo json_encode(array(
                'method' => 'DELETE',
                'status' => 'failed',
                'message' => 'Payload must not be empty'
            ));
            return;
        }

        // Check if the id passed in matches the id in the payload
        if ($id != $payload['id']) {
            echo json_encode(array(
                'method' => 'DELETE',
                'status' => 'failed',
                'message' => 'ID in payload does not match provided ID'
            ));
            return;
        }

        // Check if payload is an array
        if (!is_array($payload)) {
            // Execute delete query with single ID
            $this->db->where('id', $id);
        } else {
            // Execute delete query with multiple IDs using IN operator
            $this->db->where('id', $payload, 'IN');
        }

        // Execute delete query
        $result = $this->db->delete('tbl_to_do_List');

        // Check if query was successful
        if ($result) {
            echo json_encode(array(
                'method' => 'DELETE',
                'status' => 'success',
                'message' => 'Task Deleted Successfully',
                'data' => $id
            ));
        } else {
            echo json_encode(array(
                'method' => 'DELETE',
                'status' => 'failed',
                'message' => 'Failed to Delete Data'
            ));
        }
    }
}

//Identifier if what type of request
$request_method = $_SERVER['REQUEST_METHOD'];
// For GET,POST,PUT & DELETE Request
if ($request_method === 'GET') {
    $received_data = $_GET;
}
// Modified the condition for POST request because the data being sent from 
// AJAX functions are form-encoded data
else if ($request_method === 'POST') {
    $received_data = $_POST;
} else {
    //check if method is PUT or DELETE, and get the ids on URL
    if ($request_method === 'PUT' || $request_method === 'DELETE') {
        $request_uri = $_SERVER['REQUEST_URI'];

        $ids = null;
        $exploded_request_uri = array_values(explode("/", $request_uri));

        $last_index = count($exploded_request_uri) - 1;

        $ids = $exploded_request_uri[$last_index];
        // Payload data
        // Moved the json_decode to only trigger for PUT and DELETE requests
        $received_data = json_decode(file_get_contents('php://input'), true);
    }
}

$api = new API;

//Checking if what type of request and designating to specific functions
switch ($request_method) {
    case 'GET':
        $api->httpGet($received_data);
        break;
    case 'POST':
        $api->httpPost($received_data);
        break;
    case 'PUT':
        $api->httpPut($ids, $received_data);
        break;
    case 'DELETE':
        $api->httpDelete($ids, $received_data);
        break;
}
